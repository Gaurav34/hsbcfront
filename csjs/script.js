$(function(){
  
  
  
  
  
  // ---------- PARAMETERS ---------
  
  var randomSpeeds = function(){ return getRandomInteger(1000, 2000) } // The lower, the faster
  var delay = 50 // The higher, the more delay
  var startScreenPercentage = 0.70 // starts from 70% of the screen...
  var endScreenPercentage = 0.97 // ...till 100% (end) of the screen
  
 
  // -------------------------------
  
  
  
  // https://cdn2.iconfinder.com/data/icons/flat-icons-web/40/Heart_Fill-512.png"

  
  
  // Generates a random integer between the min and max
  var getRandomInteger = function(min, max){
    return Math.floor(Math.random() * (max-min+1)) + min
  }
  
  var fbReactions = ['angry', 'sad', 'surprise', 'happy', 'shy']
  var interval
  
  $('#button1').on('click', function(event){
    interval = setInterval(function(){
      var emojiName = $(event.target).parent().data("emoji")
      $('body').append('<img class="particle" src="../icon/E4.png" />')
      $('.particle').toArray().forEach(function(particle){
        var bounds = getRandomInteger($('body').width() * startScreenPercentage, $('body').width() * endScreenPercentage)
        $(particle).animate({ left: bounds, right: bounds}, delay, function(){
          $(particle).animate({ top: '-100%', opacity: 0}, randomSpeeds() , function(){
            $(particle).remove()
          })
        })
      }) /* forEach particle Loop close*/
      clearInterval(interval) 
    }, 1 ) /* setInterval close*/
  }) /* on('click') close*/

  

 
  $('#button2').on('click', function(event){
    interval = setInterval(function(){
      var emojiName = $(event.target).parent().data("emoji")
      $('body').append('<img class="particle" src="../icon/E3.png" />')
      $('.particle').toArray().forEach(function(particle){
        var bounds = getRandomInteger($('body').width() * startScreenPercentage, $('body').width() * endScreenPercentage)
        $(particle).animate({ left: bounds, right: bounds}, delay, function(){
          $(particle).animate({ top: '-100%', opacity: 0}, randomSpeeds() , function(){
            $(particle).remove()
          })
        })
      }) /* forEach particle Loop close*/
      clearInterval(interval) 
    }, 1 ) /* setInterval close*/
  })
 

  $('#button3').on('click', function(event){
    interval = setInterval(function(){
      var emojiName = $(event.target).parent().data("emoji")
      $('body').append('<img class="particle" src="../icon/E1.png" />')
      $('.particle').toArray().forEach(function(particle){
        var bounds = getRandomInteger($('body').width() * startScreenPercentage, $('body').width() * endScreenPercentage)
        $(particle).animate({ left: bounds, right: bounds}, delay, function(){
          $(particle).animate({ top: '-100%', opacity: 0}, randomSpeeds() , function(){
            $(particle).remove()
          })
        })
      }) /* forEach particle Loop close*/
      clearInterval(interval) 
    }, 1 ) /* setInterval close*/
  })

  $('#button4').on('click', function(event){
    interval = setInterval(function(){
      var emojiName = $(event.target).parent().data("emoji")
      $('body').append('<img class="particle" src="../icon/E2.png" />')
      $('.particle').toArray().forEach(function(particle){
        var bounds = getRandomInteger($('body').width() * startScreenPercentage, $('body').width() * endScreenPercentage)
        $(particle).animate({ left: bounds, right: bounds}, delay, function(){
          $(particle).animate({ top: '-100%', opacity: 0}, randomSpeeds() , function(){
            $(particle).remove()
          })
        })
      }) /* forEach particle Loop close*/
      clearInterval(interval) 
    }, 1 ) /* setInterval close*/
  })

  $('#button5').on('click', function(event){
    interval = setInterval(function(){
      var emojiName = $(event.target).parent().data("emoji")
      $('body').append('<img class="particle" src="../icon/E5.png" />')
      $('.particle').toArray().forEach(function(particle){
        var bounds = getRandomInteger($('body').width() * startScreenPercentage, $('body').width() * endScreenPercentage)
        $(particle).animate({ left: bounds, right: bounds}, delay, function(){
          $(particle).animate({ top: '-100%', opacity: 0}, randomSpeeds() , function(){
            $(particle).remove()
          })
        })
      }) /* forEach particle Loop close*/
      clearInterval(interval) 
    }, 1 ) /* setInterval close*/
  })

  $('#button6').on('click', function(event){
    interval = setInterval(function(){
      var emojiName = $(event.target).parent().data("emoji")
      $('body').append('<img class="particle" src="../icon/E6.png" />')
      $('.particle').toArray().forEach(function(particle){
        var bounds = getRandomInteger($('body').width() * startScreenPercentage, $('body').width() * endScreenPercentage)
        $(particle).animate({ left: bounds, right: bounds}, delay, function(){
          $(particle).animate({ top: '-100%', opacity: 0}, randomSpeeds() , function(){
            $(particle).remove()
          })
        })
      }) /* forEach particle Loop close*/
      clearInterval(interval) 
    }, 1 ) /* setInterval close*/
  })

  $('#button7').on('click', function(event){
    interval = setInterval(function(){
      var emojiName = $(event.target).parent().data("emoji")
      $('body').append('<img class="particle" src="../icon/E7.png" />')
      $('.particle').toArray().forEach(function(particle){
        var bounds = getRandomInteger($('body').width() * startScreenPercentage, $('body').width() * endScreenPercentage)
        $(particle).animate({ left: bounds, right: bounds}, delay, function(){
          $(particle).animate({ top: '-100%', opacity: 0}, randomSpeeds() , function(){
            $(particle).remove()
          })
        })
      }) /* forEach particle Loop close*/
      clearInterval(interval) 
    }, 1 ) /* setInterval close*/
  })

}) /* JQuery onDocumentReady close*/

